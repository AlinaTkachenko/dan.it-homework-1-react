import './App.scss';
import Button from './Components/Button/Button';
import ModalImage from './Components/Modal/ModalImage';
import ModalText from './Components/Modal/ModalText';

const App = () => {

  const openModal = (e) => {
    document.querySelector(`div[data-modal=${e.target.getAttribute('data-button')}]`).parentNode.classList.add('active');
  }

  return (
    <div className='wrapper'>
      <Button optionalClassNames='button-modal-first' onClick={openModal} data='modalImage'>Open first modal</Button>
      <Button optionalClassNames='button-modal-second' onClick={openModal} data='modalText'>Open second modal</Button>
      <ModalImage />
      <ModalText />
    </div>  
  );
}

export default App;
