import ModalWrapper from "./ModalComponents/ModalWrapper"
import Modal from './ModalComponents/Modal';
import ModalHeader from "./ModalComponents/ModalHeader";
import ModalBody from "./ModalComponents/ModalBody";
import ModalImg from "./ModalComponents/ModalImg";
import ModalFooter from "./ModalComponents/ModalFooter";

const ModalImage = () => {

    const closeModal = () => {
        document.querySelector(`div[data-modal="modalImage"]`).parentNode.classList.remove('active');
    }

    return (
        <ModalWrapper>
            <Modal data='modalImage'>
                <ModalImg src='image/image.png'></ModalImg>
                <ModalHeader>Product Delete!</ModalHeader>
                <ModalBody>By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.</ModalBody>
                <ModalFooter firstText='NO, CANCEL' firstClick={closeModal} secondaryText='YES, DELETE' secondaryClick={closeModal}/>
            </Modal>
        </ModalWrapper>
    )
}

export default ModalImage;