import ModalWrapper from "./ModalComponents/ModalWrapper"
import Modal from './ModalComponents/Modal';
import ModalHeader from "./ModalComponents/ModalHeader";
import ModalBody from "./ModalComponents/ModalBody";
import ModalFooter from "./ModalComponents/ModalFooter";

const ModalText = () => {

    const closeModal = () => {
        document.querySelector(`div[data-modal="modalText"]`).parentNode.classList.remove('active');
    }

    return (
        <ModalWrapper>
            <Modal data='modalText'>
                <ModalHeader>Add Product “NAME”</ModalHeader>
                <ModalBody>Description for you product</ModalBody>
                <ModalFooter firstText='ADD TO FAVORITE' firstClick={closeModal}/>
            </Modal>
        </ModalWrapper>
    )
}

export default ModalText;