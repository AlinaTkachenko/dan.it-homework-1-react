import ModalClose from "./ModalClose";

const Modal = ({data,children}) => {

    const closeModal = () => {
        document.querySelector(`div[data-modal=${data}]`).parentNode.classList.remove('active');
    }

    return (
        <div className='modal' data-modal={data}>
            <ModalClose onClick={closeModal}/> 
            {children}   
        </div>
    )
}

export default Modal;