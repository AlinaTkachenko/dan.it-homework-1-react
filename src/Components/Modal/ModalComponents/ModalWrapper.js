import './ModalWrapper.scss';

const ModalWrapper = ({children}) => {

    const closeModal = (e) => {
        e.target.classList.remove('active');
    }

    return (
        <div className='modal-wrapper' onClick={closeModal}>{children}</div>
    )
}

export default ModalWrapper;