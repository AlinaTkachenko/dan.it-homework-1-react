const ModalClose = ({onClick}) => {
    
    return (
        <div className='modal-close' onClick={onClick}>
            <img src='image/close.png' alt='close'></img>
        </div>
    )
}

export default ModalClose;