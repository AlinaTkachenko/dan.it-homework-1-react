import './Button.scss';

const Button = ({type='button', classNames='button', optionalClassNames, onClick, children, data}) => {

return (
    <button type={type} className={classNames +' '+ optionalClassNames} onClick={onClick} data-button={data}>{children}</button>
)
}

export default Button;